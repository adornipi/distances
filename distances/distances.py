"""
compute hausdorff distance between two point clouds given as txt files with x,y coordinates
the txt file names are given as arguments
"""
import click
import numpy as np
import scipy.spatial.distance as dist


def hausdorff(point_cloud1: np.ndarray, point_cloud2: np.ndarray) -> float:
    """
    compute hausdorff distance between two point clouds given as txt files with x,y coordinates

    Parameters
    ----------
    point_cloud1 : np.ndarray
        first point cloud
    point_cloud2 : np.ndarray
        second point cloud

    Returns
    -------
    float

    """
    return max(
        dist.directed_hausdorff(point_cloud1, point_cloud2)[0],
        dist.directed_hausdorff(point_cloud2, point_cloud1)[0],
    )


@click.command()
@click.argument("file1", type=click.Path(exists=True))
@click.argument("file2", type=click.Path(exists=True))
def main(file1, file2):
    data1 = np.loadtxt(file1)
    data2 = np.loadtxt(file2)
    print(hausdorff(data1, data2))


if __name__ == "__main__":
    main()
