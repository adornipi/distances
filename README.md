# Distance

This package contains utils functions to compute distances between two point clouds.

## Available distances

- Hausdorff distance

## Installation

```bash
pip install distances --index-url https://gitlab.utc.fr/api/v4/projects/12777/packages/pypi/simple
```

## Usage

### CLI

```bash
distance_computor a.txt b.txt
```

### Python

```python
from distances import hausdorff

a = [[0, 0], [1, 1], [2, 2]]
b = [[0, 0], [1, 1], [2, 2], [3, 3]]

hausdorff(a, b)
```

# License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details