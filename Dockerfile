FROM python:3.11-alpine

WORKDIR /app
RUN pip install --no-cache-dir distances==0.1.1 --index-url https://gitlab.utc.fr/api/v4/projects/12777/packages/pypi/simple
ENTRYPOINT ["distance_computor"]