import numpy as np

from distances import hausdorff


def test_hausdorff():
    assert hausdorff(np.array([[0, 0], [1, 1]]), np.array([[0, 0], [1, 1]])) == 0
